from django.shortcuts import render

apps = [
    {
        'title'          : 'Daily Output Log',
        'description'    : 'For self-reporting column loading totals and daily shift comments or issues.',
        'url'            : "cltlog-home",
    },
    {
        'title'         : 'Placeholder 1',
        'description'   : 'Basic placeholder card for design formatting and testing. Not linked to anything.',
        'url'           : 'dashboard-home'
    },
    {
        'title'         : 'Placeholder 2',
        'description'   : 'Additional placeholder.',
        'url'           : 'dashboard-home'
    },
    {
        'title'         : 'Placeholder 3',
        'description'   : 'Placeholder 3 App is currently unavailable. Please check back later.',
        'url'           : ''
    }
]
def home(request):
    context = {
        'title' :   'Dashboard Home',
        'apps'  :   apps
    }
    return render(request, 'dashboard/home.html', context)