from django.shortcuts import render

def home(request):
    context = {
        'title' : 'Daily Output Log',
    }
    return render(request, 'cltlog/home.html', context)